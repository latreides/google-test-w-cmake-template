# Introduction
This is a simple CMake based project that includes an integrated Google Test by dynamically downloading
the framework from github on demand.

## Running
There are various build targets generated in this project:

`runTests` will execute the sample test suite

`example` will execute the application being tested

`gtest`, `gtest_main`, `gmock` and `gmock_main` are internal build targets that build the Google Test framework
components individually.  These are run automatically when the `runTests` target is run, and not intended to be run
separately.
