#ifndef EXAMPLE_H
#define EXAMPLE_H
#include <string>

namespace example_namespace {
    class example
    {
    public:
        std::string ucase(const std::string &value);
    };
}
#endif //EXAMPLE_H
