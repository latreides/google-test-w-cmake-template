#include "example.h"
#include <algorithm>
namespace example_namespace
{
    std::string example::ucase(const std::string& value)
    {
        std::string new_string = std::string(value);
        std::transform(new_string.begin(), new_string.end(),new_string.begin(), std::toupper);
        return new_string;
    }
}